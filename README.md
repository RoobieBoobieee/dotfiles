# Install

```bash
# symlink the dotfiles into your home directory
./bootstrap

# only for Debian based e.g. Ubuntu, Lubuntu, Kubuntu etc.
./install.sh
```

# Extras

## Fonts

Download the Font Awesome fonts [here](https://fontawesome.com/download). Put them in `~/.fonts` and run `sudo fc-cache --force --verbose`.
Then go to [here](https://fontawesome.com/how-to-use/on-the-web/setup/using-package-managers) and run the command to set the private key for npm globally.

## Printscreen

Go to `Settings>Devices>Keyboard`. Add a new shortcut with command `flameshot gui` and bind it to the `PrtSc` button.

## Enable/rotate second screen
```bash
xrandr --auto
xrandr --output DVI-I-0 --rotate left
xrandr --output DVI-I-0 --right-of DVI-D-0
```

## Set st as default terminal
```bash
sudo update-alternatives --install /usr/bin/x-terminal-emulator x-terminal-emulator /usr/local/bin/st 0
sudo update-alternatives --config x-terminal-emulator
```

## Redirect ip addr to specific adapter
```bash
sudo ip route add 192.168.1.200/32 via 192.168.1.1 dev wlp8s0
```

## Add printer
run `system-config-printer`

## Get the class name of a window (for dwm)
```bash
xprop | awk '
        /^WM_CLASS/{sub(/.* =/, "instance:"); sub(/,/, "\nclass:"); print}
        /^WM_NAME/{sub(/.* =/, "title:"); print}'
```

