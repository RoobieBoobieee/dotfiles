# Git
alias gcs='git checkout staging'
alias gsm='git reset $(git merge-base master $(git rev-parse --abbrev-ref HEAD))'
alias gsd='git reset $(git merge-base develop $(git rev-parse --abbrev-ref HEAD))'

# Git flow
alias gf='git flow'
alias gff='git flow feature'
alias gffs='git flow feature start'
alias gfff='git flow feature finish'
alias gffp='git flow feature publish'
alias gffc='git flow feature checkout'

alias gfr='git flow release'
alias gfrs='git flow release start'
alias gfrf='git flow release finish'
alias gfrp='git flow release publish'

alias gfh='git flow hotfix'
alias gfhs='git flow hotfix start'
alias gfhf='git flow hotfix finish'
alias gfhp='git flow hotfix publish'

# Git worktree
alias gw='git worktree'

# Laravel
alias pa='php artisan'
alias p2a='php7.2 artisan'
alias p4a='php7.4 artisan'

alias ipconfig='ifconfig'

# alias node='docker run --rm -it --volume $(pwd):/app node:16 node'
alias c='docker run --rm -it --volume ~/.composer_cache:/root/composer/ --volume $(pwd):/app --volume $SSH_AUTH_SOCK:/ssh-agent --env SSH_AUTH_SOCK=/ssh-agent composer --ignore-platform-reqs'
alias copy='xclip -selection clipboard'
alias ytdl='youtube-dl -c -o "%(title)s-%(id)s.%(ext)s" -f best -x --audio-format mp3'

alias cdp='cd ~/Projects'
alias cdr='cd $(git rev-parse --show-toplevel)'

alias autotype='sleep 2.0; xdotool type "$(xclip -o -selection clipboard)"'

alias install='sudo dpkg -i'

alias ll='ls -agGhF'
alias la='ls -A'
alias l='ls -CF'

alias ifconfig="ip -c a | sed -e 's/\// \//g'"

alias b='buku --suggest'
alias bs='buku --deep'


alias lintjenkins='JENKINS_URL=https://jenkins.air.emea.group.atlascopco.com && JENKINSFILE="${JF:-jenkinsfile}" && echo          $JENKINSFILE && JENKINS_CRUMB=`curl "$JENKINS_URL/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,\":\",//crumb)"` &&        curl -X POST -H $JENKINS_CRUMB -F "jenkinsfile=<$JENKINSFILE" $JENKINS_URL/pipeline-model-converter/validate'

alias vim='nvim'
# https://gist.github.com/jgrodziski/9ed4a17709baad10dbcd4530b60dfcbb

function dnames-fn {
for ID in `docker ps | awk '{print $1}' | grep -v 'CONTAINER'`
do
    docker inspect $ID | grep Name | head -1 | awk '{print $2}' | sed 's/,//g' | sed 's%/%%g' | sed 's/"//g'
done
}

function dip-fn {
echo "IP addresses of all named running containers"

for DOC in `dnames-fn`
do
    IP=`docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}} {{end}}' "$DOC"`
    OUT+=$DOC'\t'$IP'\n'
done
echo -e $OUT | column -t
unset OUT
}

function dex-fn {
docker exec -it $1 ${2:-bash}
}

function di-fn {
docker inspect $1
}

function dl-fn {
docker logs -f $1
}

function drun-fn {
docker run -it $1 $2
}

function dcr-fn {
docker compose run $@
}

function dsr-fn {
docker stop $1;docker rm $1
}

function drmc-fn {
docker rm $(docker ps --all -q -f status=exited)
}

function drmid-fn {
imgs=$(docker images -q -f dangling=true)
[ ! -z "$imgs" ] && docker rmi "$imgs" || echo "no dangling images."
}

# in order to do things like dex $(dlab label) sh
function dlab {
docker ps --filter="label=$1" --format="{{.ID}}"
}

function dc-fn {
docker compose $*
}

alias dc=dc-fn
alias dcu="docker compose up -d"
alias dcd="docker compose down"
alias dcr=dcr-fn
alias dex=dex-fn
alias di=di-fn
alias dim="docker images"
alias dip=dip-fn
alias dl=dl-fn
alias dnames=dnames-fn
alias dps="docker ps"
alias dpsa="docker ps -a"
alias drmc=drmc-fn
alias drmid=drmid-fn
alias drun=drun-fn
alias dsp="docker system prune --all"
alias dsr=dsr-fn

alias d=docker
alias dew='docker exec webshop'
alias dec='docker exec connector'
