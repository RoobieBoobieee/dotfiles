#!/bin/sh
#!/bin/sh#include .Xresources.colors
# https://felix-kling.de/blog/2021/linux-toggle-dark-mode.html

# Determine mode
if [ "$(readlink ~/.Xresources.colors)" = ".Xresources.solarized.light" ]; then
  background=dark
else
  background=light
  export FZF_CTRL_R_OPTS="--color=light"
  export FZF_CTRL_T_OPTS="--color=light"
  export FZF_ALT_A_OPTS="--color=light"
fi

# A specific mode (light/dark) can be forced from the command line
if [ -n "$1" ] && [ "$1" != "$background" ]; then
  # This is not that intuitive but if the requested mode is different from the
  # next mode then the _current_ mode is the same as the requested one and there
  # is nothing to do
  exit 0
fi

# Update color files
# i3 and other applications that use X resources. The symlink is used to load
# the correct color scheme on startup (.Xresources includes .Xresources.colors
# via #include .Xresources.colors)
ln -sf ".Xresources.solarized.$background" ~/.Xresources.colors
# Overwrite color configuration
# xrdb -merge ~/.Xresources.colors
# Vim
echo "set background=$background" > ~/.vimrc.colors
# Rofi
cp ~/.config/rofi/themes/solarized-$background.rasi ~/.config/rofi/themes/active.rasi
# For triggering dark themes in GTK apps. Requires `gnome-themes-extra` to be
# installed (for adwaita-dark). This is primarily for Firefox
if [ $background = dark ]; then
  sed -i s/Adwaita/Adwaita-dark/ ~/.xsettingsd
else
  sed -i s/Adwaita-dark/Adwaita/ ~/.xsettingsd
fi


# Reload
# Update WM background
xsetroot -solid "$(xrdb -query | grep 'background' | head -n1 | cut -f 2)"
# Update Gtk apps
killall -HUP xsettingsd
# Update terminal emulator
kitty +kitten themes --reload-in=all "Solarized $(echo $background | sed 's/./\U&/')"
# Vim watches ~/.vimrc.color and reloads itself

