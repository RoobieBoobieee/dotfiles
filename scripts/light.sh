#!/bin/sh

sed -i 's/theme\.conf/theme-light.conf/' ~/.config/kitty/kitty.conf
sed -i 's/background=dark/background=light/' ~/.vimrc
