#!/usr/bin/env bash

source ./scripts/functions.sh

# install new git / ubuntu
sudo add-apt-repository -y ppa:git-core/ppa > /dev/null

# Add php 7.2
sudo add-apt-repository ppa:ondrej/php

# add yarn
installed "yarn"
if [[ $? -eq 1 ]]; then
  curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
  echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list > /dev/null
fi
# add node.js
installed "nodejs"
if [[ $? -eq 1 ]]; then
  curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash - > /dev/null
fi

sudo -qq apt update -y > /dev/null

# Install mysql without interaction
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

packagelist=(
  # repo-tools
  tig
  git-flow
  # Python
  python
  python3-pip
  # Javascript
  yarn
  nodejs
  # Web
  apache2
  mysql-server
  php7.4
  php7.4-bcmath
  php7.4-bz2
  php7.4-intl
  php7.4-gd
  php7.4-mbstring
  php7.4-mysql
  php7.4-zip
  php7.4-fpm
  php7.4-dom
  php7.4-curl
  libapache2-mod-php
  supervisor

  # General development
  ctags
)

sudo apt -qq install -y ${packagelist[@]}

# Enable modrewrite for apache
sudo a2enmod rewrite
sudo a2enmod headers

# Make dir if it doesn't exist already
mkdir -p zsh/.oh-my-zsh

# git-flow autocomplete
if [ ! -f "zsh/.oh-my-zsh/git-flow-completion.zsh" ]; then
  wget https://raw.githubusercontent.com/bobthecow/git-flow-completion/master/git-flow-completion.zsh -O zsh/.oh-my-zsh/git-flow-completion.zsh
fi

# zsh - autocomplete
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

# Install composer
if [ ! -f "/usr/bin/composer" ]; then
  curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/bin
  sudo ln -s /usr/bin/composer.phar /usr/bin/composer
fi

composer global install

# Install nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
. ~/.nvm/nvm.sh
nvm install node

# https://github.com/guard/listen/wiki/Increasing-the-amount-of-inotify-watchers
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
