#!/usr/bin/env bash

source ./scripts/functions.sh

# Theme
sudo add-apt-repository -y ppa:system76/pop

sudo -qq apt update -y

packagelist=(
  pop-theme

  gnome-tweak-tool
)

sudo apt install -y ${packagelist[@]}

# Theme settings
gsettings set org.gnome.desktop.interface icon-theme 'Pop'
gsettings set org.gnome.desktop.interface gtk-theme 'Pop-dark'
gsettings set org.gnome.desktop.interface cursor-theme 'DMZ-White'

# Move buttons to the left
gsettings set org.gnome.desktop.wm.preferences button-layout 'close,minimize,maximize:'

# Turn on night light
gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true
gsettings set org.gnome.settings-daemon.plugins.color night-light-temperature 4000
gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-from 0
gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-to 23.983333333333334

# Hide desktop icons
gsettings set org.gnome.desktop.background show-desktop-icons false

# Set background
gsettings set org.gnome.desktop.background picture-uri $(realpath ./background/background.jpg)

# Swap esc and caps
gsettings set org.gnome.desktop.input-sources xkb-options "['caps:swapescape']"

# Unbind screenshot button
# TODO: rebind with flameshot
# Got to Settings -> Devices -> Keyboard and scroll to the end. Press +. add 'flameshot gui'
gsettings set org.gnome.settings-daemon.plugins.media-keys screenshot '[]'

# Enable rightclick on touchpad without hardware buttons
gsettings set org.gnome.desktop.peripherals.touchpad click-method areas

